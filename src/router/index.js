import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layout/index.vue'
import Login from '@/views/Login/index.vue'
import Register from '@/views/Register/index.vue'

const routes = [

    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/',
        component: Layout,
        redirect: '/home',
        children: [{
            path: 'home',
            name: 'home',
        }]
    },
]


const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router